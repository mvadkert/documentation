---
title: "Shared Cyborg Project Documentation"
cascade:
  type: "docs"
---

## Usage

Use the navigation bar on the left to navigate the documentation.

## Writing documentation

New documentation is always welcome! Start by cloning the documentation
repository and installing the necessary requirements:

```console
git submodule update --init --recursive
npm i --no-package-lock
```

Add your documentation as a markdown file inside the appropriate directory
under `content/`. Every file should end in `.md`. We use [Hugo] with the
[Docsy] theme to build the site.

You can preview your documentation live by running `hugo serve`. A local
webserver will start and it will automatically refresh your browser each time
you save a change.

Commit your changes and push them to a new branch. Start a merge request and a
team member will review your change.

[Hugo]: https://gohugo.io/
[Docsy]: https://www.docsy.dev/
