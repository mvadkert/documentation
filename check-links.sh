#!/bin/bash

set -euo pipefail

hugo serve &
trap 'trap - EXIT && kill -- -$$' EXIT
sleep 3

muffet http://localhost:1313/documentation/ \
    --max-connections=16 \
    --timeout 60 \
    --ignore-fragments \
    --exclude 'source.redhat.com'

# if no dangling links were found, shut down normally
kill %1
trap - EXIT
