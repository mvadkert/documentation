# Shared documentation of the Cyborg projects

This repository contains the source for the Cyborg [documentation website].

To edit it:

- browse the [content/](content) directory and click on `Edit`/`Web IDE` at the
  top of a file
- click on the `Edit on GitLab` link in the top right hand corner of a page on
  the [documentation website]

For local testing, start a web server with

```bash
git submodule update --init --recursive
npm i --no-package-lock
hugo serve
```

[documentation website]: https://cyborg-infra.gitlab.io/documentation/
